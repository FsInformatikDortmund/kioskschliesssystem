def checkCard(uid):
	try:
		cnx = mysql.connector.connect(user=config.get('mySQL', 'user'),password=config.get('mySQL', 'password'), host=config.get('mySQL', 'host'), database=config.get('mySQL', 'database'))
		cursor = cnx.cursor(buffered=True) # buffered is needed for pre fetch rowcounter
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			logging.error("ACCESS DENIED")
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			logging.error("DB ERROR")
		else:
			logging.error(err)
	else:
		cursor.execute(("SELECT id FROM cards WHERE card = %(card)s"), {'card': uid})
		if cursor.rowcount == 0:
			return False
		else:
			return True
		cursor.close()
		cnx.close()
	return False

def add_trigger(triggername,cardid=None):
	try:
		cnx = mysql.connector.connect(user=config.get('mySQL', 'user'),password=config.get('mySQL', 'password'), host=config.get('mySQL', 'host'), database=config.get('mySQL', 'database'))
		cursor = cnx.cursor()
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			logging.error("ACCESS DENIED")
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			logging.error("DB ERROR")
		else:
			logging.error(err)
	else:
		if cardid == None:
			cursor.execute(("INSERT INTO trigger_logs (triggername) VALUES (%(triggername)s"), {'triggername': triggername})
		else:
			cursor.execute(("INSERT INTO trigger_logs (triggername, cardid) VALUES (%(triggername)s, %(cardid)s)"), {'triggername': triggername,'cardid': cardid})
		cnx.commit()
		cursor.close()
		cnx.close()