#!/usr/bin/python

from configparser import SafeConfigParser
from array import array
import signal
import time
import logging
import mysql.connector
from mysql.connector import errorcode
import RPi.GPIO as GPIO
import MFRC522
from helper import uidToString
from sql import checkCard, add_trigger

logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",format="%(asctime)-15s %(levelname)-8s %(message)s")
continue_reading = True
MIFAREReader = MFRC522.MFRC522()
lastId = -1;

#config
config = SafeConfigParser()
config.read('config.ini')

#gpio setup
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
relayPin = int(config.get('primaryPinout', 'relay'))
GPIO.setup(relayPin, GPIO.OUT)

#gpio setup for triggers
triggerNames = []
triggerPins = []
triggerStates = []
for x in config.options('triggerPinout')[:]:
	triggerNames.append(x)
	triggerPins.append(int(config.get('triggerPinout', x)))
	GPIO.setup(int(config.get('triggerPinout', x)), GPIO.IN, pull_up_down=GPIO.PUD_UP)
	triggerStates.append(GPIO.input(int(config.get('triggerPinout', x))))

#Cleanup
def end_read(signal,frame):
	global continue_reading
	continue_reading = False
	GPIO.cleanup()

signal.signal(signal.SIGINT, end_read)

#Main Loop
while continue_reading:
	#NFC
	(status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
	if status == MIFAREReader.MI_OK:
		(status,uid) = MIFAREReader.MFRC522_Anticoll()
		if status == MIFAREReader.MI_OK:
			if checkCard(uidToString(uid)):
				add_trigger("RFID-OPEN",uidToString(uid))
				GPIO.output(relayPin, GPIO.HIGH)
				time.sleep(1)
				GPIO.output(relayPin, GPIO.LOW)
				time.sleep(0.5)
			else:
				GPIO.output(relayPin, GPIO.LOW)
				add_trigger("RFID-REJECT",uidToString(uid))
	
	#TRIGGER
	for x in range(0, len(triggerNames)):
		inputState = GPIO.input(triggerPins[x])
		if inputState != triggerStates[x]:
			triggerStates[x] = inputState
			if inputState == True :
				add_trigger(triggerNames[x]+"-OPENED")
			else:
				add_trigger(triggerNames[x]+"-CLOSED")
	time.sleep(0.5)