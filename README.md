# documentation
documentation.pdf eine grobe erklärung von allem

Schaltplan.fzz ein minimaler Schaltplan welcher mit Fritzing zu öffnen ist

# program
config.ini.skel skeleton for config.ini

kioskschliesssystem.py the program itself

# setup
installDB.sql setup DB and tables

installUser.sql setup User with adequate rights on DB and tables

setup.sh for local ClientSide setup