#!/bin/bash

# Needed Packages
sudo apt-get install python3-dev python3-rpi.gpio git

# SPI SETUP
if grep -Fxq "device_tree_param=spi=on" /boot/config.txt
then
    echo "Device tree param already on."
else
    echo "device_tree_param=spi=on" | sudo tee --append /boot/config.txt 
fi
if grep -Fxq "device_tree_param=spi=on" /boot/config.txt
then
    echo "dtoverlay already set."
else
	echo "dtoverlay=spi-bcm2708" | sudo tee --append /boot/config.txt 
fi
if grep -Fxq "dtparam=spi" /boot/config.txt
then
	sudo sed -i 's/#dtparam=spi=on/dtparam=spi=on/g' /boot/config.txt;
	sudo sed -i 's/#dtparam=spi=off/dtparam=spi=on/g' /boot/config.txt;
	sudo sed -i 's/dtparam=spi=off/dtparam=spi=on/g' /boot/config.txt;
else
	echo "dtparam=spi=on" | sudo tee --append /boot/config.txt 
fi


# SPI libary
git clone https://github.com/lthiery/SPI-Py.git
cd SPI-Py
sudo python3 setup.py install
cd ..
rm -Rf SPI-Py

# NFC libary
git clone https://github.com/mxgxw/MFRC522-python.git
mv MFRC522-python/MFRC522.py ../program/
rm -Rf MFRC522-python
2to3 -w ../program/MFRC522.py

# Config
if [ -f ../program/config.ini ]; then
   echo "Config already exists."
else
	cp ../program/config.ini.skel ../program/config.ini
	nano ../program/config.ini
fi
