CREATE DATABASE IF NOT EXISTS kioskschliesssystem;

USE kioskschliesssystem;

CREATE TABLE IF NOT EXISTS `cards` (
	`id` int(10) NOT NULL AUTO_INCREMENT,
	`name` varchar(30) NOT NULL,
	`card` varchar(18) NOT NULL,
	PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `trigger_logs` (
	`id` int(20) NOT NULL AUTO_INCREMENT,
	`triggername` varchar(30) NOT NULL,
	`triggertime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`cardid` varchar(30) DEFAULT NULL,
	PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;
