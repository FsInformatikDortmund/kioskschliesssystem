#Client User
GRANT USAGE ON *.* TO 'kioskschliesssys'@'%' IDENTIFIED BY PASSWORD '*845A9ADD7E1A82B6459804066B3A45D0025897B6'; #PW=PASSWORD
GRANT INSERT (cardid, triggername) ON `kioskschliesssystem`.`trigger_logs` TO 'kioskschliesssys'@'%';
GRANT SELECT (card, id) ON `kioskschliesssystem`.`cards` TO 'kioskschliesssys'@'%';

#Admin User
GRANT USAGE ON *.* TO 'kioskaelteste'@'%' IDENTIFIED BY PASSWORD '*845A9ADD7E1A82B6459804066B3A45D0025897B6'; #PW=PASSWORD
GRANT SELECT ON `kioskschliesssystem`.`trigger_logs` TO 'kioskaelteste'@'%';
GRANT SELECT,INSERT,UPDATE,DELETE ON `kioskschliesssystem`.`cards` TO 'kioskaelteste'@'%';